﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace FootballExercise
{
    public static class LadderLoader
    {
        // regular expressions for validating text file structure
        private const string HeaderRegEx = @"^\s*Team\s+P\s+W\s+L\s+D\s+F\s+-\s+A\s+Pts\s*$";
        private const string EntryRegEx = @"^\s*\d+.\s\w+(\s+\d+){5}\s+-(\s+\d+){2}\s*$";

        public static Ladder Load(string fileName)
        {
            // basic validation
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("File not found: " + fileName);
            }

            if (File.ReadLines(fileName).Count() < 2)
            {
                throw new InvalidDataException("Invalid data file: Expecting more than 1 line");
            }

            var header = File.ReadLines(fileName).First();
            if (!ValidHeader(header))
            {
                throw new InvalidDataException("Header invalid: " + header);
            }

            var ladder = new Ladder
            {
                Teams = File.ReadAllLines(fileName).Skip(1) // skip header
                    .Where(line => !line.Trim().StartsWith("---") && line.Trim() != "") // ignore empty lines, or separator lines (----)
                    .Select(line => ParseEntry(line)) // parse each remainder
                    .ToList()
            };

            if (ladder.Teams.Count() == 0)
            {
                throw new InvalidDataException("Data file does not contain any valid team listings");
            }

            return ladder;
        }

        private static bool ValidHeader(string s)
        {
            Regex regex = new Regex(HeaderRegEx);

            return regex.Match(s).Success;
        }

        private static Team ParseEntry(string s)
        {
            Regex regex = new Regex(EntryRegEx);

            if (regex.Match(s).Success)
            {
                var parts = s.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                return new Team
                {
                    Position = Int32.Parse(parts[0].Remove(parts[0].Length - 1)),
                    Name = parts[1],
                    Played = Int32.Parse(parts[2]),
                    Won = Int32.Parse(parts[3]),
                    Lost = Int32.Parse(parts[4]),
                    Drawn = Int32.Parse(parts[5]),
                    GoalsFor = Int32.Parse(parts[6]),
                    GoalsAgainst = Int32.Parse(parts[8]),
                    Points = Int32.Parse(parts[9])
                };
            }
            else
            {
                throw new InvalidDataException("Team entry invalid: " + s);
            }
        }
    }
}
