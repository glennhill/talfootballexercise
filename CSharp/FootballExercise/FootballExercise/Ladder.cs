﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace FootballExercise
{
    public class Ladder
    {
        public List<Team> Teams { get; set; }

        public Team SmallestForAgainstDifference()
        {
            return Teams.OrderBy(t => Math.Abs(t.GoalsFor - t.GoalsAgainst)).First();
        }

        public List<Team> SmallestForAgainstDifferenceMultiple()
        {
            var min = Teams.Min(t => Math.Abs(t.GoalsFor - t.GoalsAgainst));
            return Teams.Where(t => Math.Abs(t.GoalsFor - t.GoalsAgainst) == min).ToList();
        }
    }
}
