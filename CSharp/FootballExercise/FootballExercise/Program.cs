﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var ladder = LadderLoader.Load("football.dat");
                
                Console.WriteLine("Team(s) with smallest difference in goals for/against");
                Console.WriteLine("-----------------------------------------------------");
                ladder.SmallestForAgainstDifferenceMultiple().ForEach(
                    t => Console.WriteLine(String.Format("{0} {1}", t.Name, Math.Abs(t.GoalsFor - t.GoalsAgainst).ToString()))
                );
            }
            catch (Exception e)
            {
               Console.WriteLine("Error: " + e.Message);
            }

            Console.ReadKey();
        }
    }
}
