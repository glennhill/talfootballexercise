﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FootballExercise;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace FootballExerciseUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InstantiateLadder()
        {
            var ladder = LadderLoader.Load("football_valid.dat");   
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void InstantiateInvalidLadder()
        {
            var ladder = LadderLoader.Load("football_invalid.dat");
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void LadderFileNotFound()
        {
            var ladder = LadderLoader.Load("thisfiledoesntexist.dat");
        }

        [TestMethod]
        public void SmallestForAgainstDifferenceTest()
        {
            var ladder = LadderLoader.Load("football_valid.dat");

            List<Team> t = ladder.SmallestForAgainstDifferenceMultiple();

            Assert.AreEqual(t.First().Name, "Aston_Villa");
        }
    }
}
