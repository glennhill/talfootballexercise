﻿Public Class Ladder

    Public Property Teams As List(Of Team)

    Public Function SmallestForAgainstDifference() As Team
        SmallestForAgainstDifference = Teams.OrderBy(Function(t) Math.Abs(t.GoalsFor - t.GoalsAgainst)).First()
    End Function

    Public Function SmallestForAgainstDifferenceMultiple() As List(Of Team)
        Dim min = Teams.Min(Function(t) Math.Abs(t.GoalsFor - t.GoalsAgainst))
        SmallestForAgainstDifferenceMultiple = Teams.Where(Function(t) Math.Abs(t.GoalsFor - t.GoalsAgainst) = min).ToList()
    End Function

End Class
