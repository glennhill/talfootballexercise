﻿Public Class Team

    Public Property Position As Integer
    Public Property Name As String
    Public Property Played As Integer
    Public Property Won As Integer
    Public Property Lost As Integer
    Public Property Drawn As Integer
    Public Property GoalsFor As Integer
    Public Property GoalsAgainst As Integer
    Public Property Points As Integer

End Class
