﻿Module Module1

    Sub Main()
        Try
            Dim ladder As Ladder = LadderLoader.Load("football.dat")

            Console.WriteLine("Team(s) with smallest difference in goals for/against")
            Console.WriteLine("-----------------------------------------------------")
            ladder.SmallestForAgainstDifferenceMultiple().ForEach(
                Sub(t) Console.WriteLine(String.Format("{0} {1}", t.Name, Math.Abs(t.GoalsFor - t.GoalsAgainst).ToString))
            )
        Catch ex As Exception
            Console.WriteLine("Error: " + ex.Message)
        End Try

        Console.ReadKey()
    End Sub

End Module
