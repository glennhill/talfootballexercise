﻿Imports System.IO
Imports System.Text.RegularExpressions

Public Class LadderLoader

    ' regular expressions for validating text file structure
    Private Const HeaderRegEx As String = "^\s*Team\s+P\s+W\s+L\s+D\s+F\s+-\s+A\s+Pts\s*$"
    Private Const EntryRegEx As String = "^\s*\d+.\s\w+(\s+\d+){5}\s+-(\s+\d+){2}\s*$"

    Public Shared Function Load(fileName As String) As Ladder
        ' basic validation first
        If Not File.Exists(fileName) Then
            Throw New FileNotFoundException("File not found: " + fileName)
        End If

        If File.ReadLines(fileName).Count() < 2 Then
            Throw New InvalidDataException("Invalid data file: Expecting more than 1 line")
        End If

        Dim header As String = File.ReadLines(fileName).First()
        If Not ValidHeader(header) Then
            Throw New InvalidDataException("Header invalid: " + header)
        End If

        ' Load ladder (parsing each entry), ignoring the header, empty lines, and separator lines 
        Dim ladder = New Ladder With {
            .Teams = File.ReadAllLines(fileName).Skip(1) _
                        .Where(Function(line) Not line.Trim().StartsWith("---") And line.Trim() <> "") _
                        .Select(Function(line) ParseEntry(line)) _
                        .ToList()
        }

        If ladder.Teams.Count = 0 Then
            Throw New InvalidDataException("Data file does not contain any valid team listings")
        End If

        Load = ladder
    End Function

    Private Shared Function ValidHeader(s As String) As Boolean
        Dim regex As Regex = New Regex(HeaderRegEx)

        ValidHeader = regex.Match(s).Success
    End Function

    Private Shared Function ParseEntry(s As String) As Team
        Dim regex As Regex = New Regex(EntryRegEx)

        If regex.Match(s).Success Then
            Dim parts() As String = s.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)

            ParseEntry = New Team With {
                            .Position = Int32.Parse(parts(0).Remove(parts(0).Length - 1)), _
                            .Name = parts(1), _
                            .Played = parts(2), _
                            .Won = parts(3), _
                            .Lost = parts(4), _
                            .Drawn = parts(5), _
                            .GoalsFor = parts(6), _
                            .GoalsAgainst = parts(8), _
                            .Points = parts(9)
                        }
        Else
            Throw New InvalidDataException("Team entry invalid: " + s)
        End If
    End Function
End Class
